BEGIN { $| = 1; print "1..1\n"; }

use Business::OnlinePayment;

my $tx = new Business::OnlinePayment("IPaymentTPG",
);

$tx->content(
    login          => 'testing', #CHANGE THESE TO TEST
    password       => 'testing',
    type           => 'VISA',
    action         => 'Normal Authorization',
    description    => 'Business::OnlinePayment::IPaymentTPG visa test',
    amount         => '0.01',
    first_name     => 'Tofu',
    last_name      => 'Beast',
    address        => '123 Anystreet',
    city           => 'Anywhere',
    state          => 'UT',
    zip            => '84058',
    country        => 'US',
    email          => 'ivan-linkpoint@420.am',
#    card_number    => '4007000000027',
    card_number    => '0000000000000',
    expiration     => '12/2002',
    result         => 'DECLINE',
);

$tx->test_transaction(1);

$tx->submit();

if($tx->is_success()) {
    print "not ok 1\n";
    $auth = $tx->authorization;
    warn "********* $auth ***********\n";
} else {
    print "ok 1\n";
    warn '***** '. $tx->error_message. " *****\n";
    exit;
}

